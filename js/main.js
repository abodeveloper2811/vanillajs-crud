var myToastDelete = Toastify({
    text: "Talaba o'chirildi !!!",
    duration: 3000
});

var myToastEdit = Toastify({
    text: "Talaba yangilandi !!!",
    duration: 3000
});


var myToastCreate = Toastify({
    text: "Yangi talaba yaratildi !!!",
    duration: 3000
});


let students = [
    {
        firstName: 'Abbos',
        lastName: 'Ibragimov',
        group: '411-19',
        age: 21
    },
    {
        firstName: 'Asadbek',
        lastName: 'Suvonov',
        group: '072-20',
        age: 20
    }
];

let edition = false;
let editedIndex = null;

function readStudents() {

    document.getElementById('studentList').innerHTML = '';

    for (let i = 0; i < students.length; i++) {

        document.getElementById('studentList').innerHTML +=
            `
                <tr>
                    <td>${i + 1}</td>
                    <td>${students[i].firstName}</td>
                    <td>${students[i].lastName}</td>
                    <td>${students[i].group}</td>
                    <td>${students[i].age}</td>
                    <td class="d-flex justify-content-around">
                        <button onclick="deleteStudent(${i})" class="btn btn-danger btn-sm">DELETE</button>
                        <button onclick="editStudent(${i})" class="btn btn-warning btn-sm">EDIT</button>
                    </td>
                </tr>
            `;
    }

};

readStudents();

function createStudent(event) {
    event.preventDefault();

    let newStudent = {
        firstName: event.target.firstName.value,
        lastName: event.target.lastName.value,
        group: event.target.group.value,
        age: event.target.age.value,
    };

    if (edition === true){
        students[editedIndex] = newStudent;

        myToastEdit.showToast();

        edition = false;
        editedIndex = null;
    }
    else {
        students.push(newStudent);
        myToastCreate.showToast();
    }

    document.forms['crudForm'].reset();

    readStudents();
}


function deleteStudent(index) {

    setTimeout(function () {
        students.splice(index, 1);
        readStudents();

        myToastDelete.showToast();
    }, 1000);

};

function editStudent(index) {

    edition = true;
    editedIndex = index;

    document.forms['crudForm'].firstName.value = students[index].firstName;
    document.forms['crudForm'].lastName.value = students[index].lastName;
    document.forms['crudForm'].group.value = students[index].group;
    document.forms['crudForm'].age.value = students[index].age;
}


